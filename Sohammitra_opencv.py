# import the necessary packages
from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import cv2
import numpy as np

#setting up camera properties
camera = PiCamera()
camera.resolution = (640, 480)
camera.framerate = 60
camera.rotation = 180
rawCapture = PiRGBArray(camera, size = (640, 480))
avg = None
time.sleep(2)

#get video 
for f in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
    # grab the raw array representation of the image
    frame = f.array
    
    # convert imags to grayscale &  blur the result
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(gray, (21, 21), 0)
    
    # inittialize avg if it hasn't been done
    if avg is None:
        avg = gray.copy().astype("float")
        rawCapture.truncate(0)
        continue
    
    # accumulate the weighted average between the current frame and
    # previous frames, then compute the difference between the current
    # frame and running average
    cv2.accumulateWeighted(gray, avg, 0.05)
    frameDelta = cv2.absdiff(gray, cv2.convertScaleAbs(avg))

    # convert the difference into binary & dilate the result to fill in small holes
    thresh = cv2.threshold(frameDelta, 25, 255, cv2.THRESH_BINARY)[1]
    thresh = cv2.dilate(thresh, None, iterations=2)
    
    # show the result
    cv2.imshow("Delta + Thresh", thresh)

    # further processing the image to make detection clearer
    contours, hierarchy = cv2.findContours(thresh.copy(),cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    
    # find the moving object 
    if len(contours) > 0:
        areas = [cv2.contourArea(c) for c in contours]
        max_index = np.argmax(areas)
        cnt=contours[max_index]   

        # rectangle
        x,y,w,h = cv2.boundingRect(cnt)
        cv2.rectangle(frame,(x,y),(x+w,y+h),(0,255,0),2)
        area = cv2.contourArea(cnt)

        # print area to the terminal
        print(area)
    
        # add text to the frame
        cv2.putText(frame, "Moving object ", (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
        
    # show the frame
    cv2.imshow("Video", frame)   
    rawCapture.truncate(0)

    # exit live feed 
    key = cv2.waitKey(1) & 0xFF
    if key == ord('q'):
        break
    
cv2.destroyAllWindows()
